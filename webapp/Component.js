jQuery.sap.declare("retail.store.receiveproducts1.ZRT_RECV_PRODS1Extension.Component");

// use the load function for getting the optimized preload file if present
sap.ui.component.load({
	name: "retail.store.receiveproducts1",
	// Use the below URL to run the extended application when SAP-delivered application is deployed on SAPUI5 ABAP Repository
	url: "/sap/bc/ui5_ui5/sap/RT_RECV_PRODS1"
		// we use a URL relative to our own component
		// extension application is deployed with customer namespace
});

this.retail.store.receiveproducts1.Component.extend("retail.store.receiveproducts1.ZRT_RECV_PRODS1Extension.Component", {
	metadata: {
		manifest: "json"
	}
});